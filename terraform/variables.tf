variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {
  default = "us-east-2"
}

variable "app_ami" {
  default = "ami-099fbff0b6a014cf2"
}
