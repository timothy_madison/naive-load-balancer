module "app" {
  source = "./app"
  private_subnets = "${module.network.private_subnets}"
  public_subnets = "${module.network.public_subnets}"
  app_ami = "${var.app_ami}"
  main_vpc = "${module.network.main_vpc}"
}
module "network" {
  source = "./network"
}
