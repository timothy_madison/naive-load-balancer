resource "aws_security_group" "app_lb" {
  name = "app_lb"
  description = "Allow HTTP inbound."
  vpc_id = "${var.main_vpc}"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 80
    to_port = 80
    protocol = "TCP"
    cidr_blocks = ["10.0.0.0/16"]
  }
}

resource "aws_security_group" "app" {
  name = "app"
  description = "Allow HTTP inbound from ALB."
  vpc_id = "${var.main_vpc}"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "TCP"
    security_groups = ["${aws_security_group.app_lb.id}"]
  }
}
