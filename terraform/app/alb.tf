resource "aws_lb" "app" {
  name = "app"
  internal = false
  load_balancer_type = "application"
  security_groups = ["${aws_security_group.app_lb.id}"]
  subnets = ["${var.public_subnets}"]
}

resource "aws_lb_listener" "app" {
  load_balancer_arn = "${aws_lb.app.arn}"
  port = 80
  protocol = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.app.arn}"
    type = "forward"
  }
}

resource "aws_lb_target_group" "app" {
  name = "app"
  port = 80
  protocol = "HTTP"
  vpc_id = "${var.main_vpc}"
}
