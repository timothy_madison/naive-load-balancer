resource "aws_autoscaling_group" "app" {
  name = "app"
  min_size = 2
  max_size = 2
  desired_capacity = 2
  health_check_grace_period = 300
  health_check_type = "ELB"
  force_delete = true
  launch_configuration = "${aws_launch_configuration.app.name}"
  target_group_arns = ["${aws_lb_target_group.app.arn}"]
  vpc_zone_identifier = ["${var.private_subnets}"]
}

resource "aws_launch_configuration" "app" {
  name = "app"
  image_id = "${var.app_ami}"
  instance_type = "t2.micro"
  security_groups = ["${aws_security_group.app.id}"]
}
