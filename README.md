# A Naive Application Load-Balancer in Terraform

This project is to serve as a sample starting place for building a highly-available web application in AWS. It is described as "naive" because it only works with stateless applications. A stateful application would require a data store of some kind to be located outside this project and configured in the AMI.

## Getting Started



### Prerequisites

This project requires Terraform, and an AWS account.

```
git clone https://github.com/kamatama41/tfenv.git ~/.tfenv
export PATH="$HOME/.tfenv/bin:$PATH
tfenv install 0.11.7
terraform --version
```

### Installing

Just clone this repository and edit two configuration files.

terraform/terraform.tfstate (needs to be created)

```
aws_access_key = "some_key"
aws_secret_key = "some_key"
```

And terraform/variables.tf

```
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {
  default = "us-east-2"
}

variable "app_ami" {
  default = "your_app_ami_here"
}
```

## Deployment

This part is pretty easy, just run:

```
terraform apply --auto-approve
```

Assuming that your IAM user has the right permissions, you'll get a new ALB+ASG in a new VPC. From here you can adapt the environment manually to your needs. To run a LAMP app for example, you would want to ensure that all state is stored in a database, create one in RDS, allow outbound DB connections in the "app" security group, and reconfigure your AMI to use this non-local database.
